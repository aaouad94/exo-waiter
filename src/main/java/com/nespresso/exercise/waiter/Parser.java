package com.nespresso.exercise.waiter;

import java.util.List;

/**
 * Created by admin on 15/12/2016.
 */
public interface Parser {

    String parse(String personOrder) throws ParserException;
}

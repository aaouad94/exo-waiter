package com.nespresso.exercise.waiter;

/**
 * Created by admin on 15/12/2016.
 */
public class ParserException extends Exception{
    public ParserException(String msg){
        super(msg);
    }
}

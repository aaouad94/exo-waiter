package com.nespresso.exercise.waiter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 15/12/2016.
 */
public class DefaultParser implements Parser {

    public String parse(String personOrder) throws ParserException {
        final String[] split = personOrder.split(":");
        if (split.length == 2) {
            return split[1];
        } else {
            throw new ParserException("Error parsing");
        }

    }
}

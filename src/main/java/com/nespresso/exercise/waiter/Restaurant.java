package com.nespresso.exercise.waiter;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {

    private List<Table> tables=new ArrayList<Table>();

    private int incIdTable=0;


    public int initTable(int sizeOfTable) {
        int idtable=createTable(sizeOfTable);
        incIdTable++;
        return idtable;
    }

    private int createTable(int sizeOfTable){
        tables.add(new Table(sizeOfTable,incIdTable));
        return incIdTable;
    }

    public void customerSays(int tableId, String message) {
        final Table table = tables.get(tableId);
        try {
            table.customerSays(message);
        } catch (ParserException e) {
            e.printStackTrace();
        }
    }

    public String createOrder(int tableId) {
        String dishsChain;
        final Table table = tables.get(tableId);
        String orders = table.createOrder();

        return orders;

    }
}

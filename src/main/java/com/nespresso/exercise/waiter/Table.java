package com.nespresso.exercise.waiter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 15/12/2016.
 */
public class Table {

    private int sizeOfTable;

    private int id;

    Parser parser=new DefaultParser();

    private List<String> dishs=new ArrayList<String>();

    public Table(int sizeOfTable, int id) {
        this.sizeOfTable = sizeOfTable;
        this.id = id;
    }
    public String createOrder(){
        StringBuilder returnValue=new StringBuilder();
        if(sizeOfTable==dishs.size()){
            for(int i=0;i<dishs.size();i++){
                returnValue.append(dishs.get(i));
                if(i!=dishs.size()-1){
                    returnValue.append(",");
                }
            }
        }else{
            final int nbMissing=sizeOfTable-dishs.size()+1;
            return "MISSING "+nbMissing;
        }
        return returnValue.toString().trim();
    }
    public void customerSays(String message) throws ParserException {
        String dish = parser.parse(message);
        if(!dishs.isEmpty()){
            final String lastDish = dishs.get(dishs.size() - 1);
            if(dish.equals(" Same")){
                dishs.add(new String(lastDish));
            }else{
                dishs.add(dish);
            }
        }else{
            dishs.add(dish);
        }

    }
}
